export class Marcados {
  idmarcados: number;
  carnetIdentidad: string;
  fecha: string;
  hora: string;
  observacion: string;
  idempleados: number;
}
