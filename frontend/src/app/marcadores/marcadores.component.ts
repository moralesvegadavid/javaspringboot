import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Empleados } from '../empleados/empleados';
import { EmpleadosService } from '../empleados/empleados.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Marcados } from './marcados';
import { MarcadosService } from './marcados.service';

@Component({
  selector: 'app-marcadores',
  templateUrl: './marcadores.component.html',
  styleUrls: ['./marcadores.component.scss']
})
export class MarcadoresComponent implements OnInit {

  private IdEmpleado: number;
  public empleadoCurrent: Empleados = new Empleados();

  marcados: Marcados[] = [];
  displayedColumns: string[] = [
    'No',
    'Fecha',
    'Hora',
    'Observacion',
  ];
  dataSource!: MatTableDataSource<any>;

  constructor(
    private empleadosService: EmpleadosService,
    private marcadosService: MarcadosService,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.IdEmpleado = params['idempleados'];
      this.getEmpleadoById();
      this.getMarcadores('all');
    });

  }

  getMarcadores(type: 'all' | 'date', value: string = '') {
    this.marcadosService
      .getAll()
      .subscribe((marcados) => {
        this.marcados = marcados;
        console.log(this.marcados);
        console.log(this.IdEmpleado);

        let FilterMarcados = (type === 'all') ?
          this.marcados.filter((marcadosF: Marcados) => marcadosF.idempleados == this.IdEmpleado)
          : this.marcados.filter((marcadosF: Marcados) => marcadosF.fecha == value && marcadosF.idempleados == this.IdEmpleado);

        this.dataSource = new MatTableDataSource(FilterMarcados);
      });
  }

  filterMarcadosByFecha(event: any) {

    const FilterFecha = (event.target as HTMLInputElement).value;
    let TypeSearch: 'all' | 'date' = (!FilterFecha) ? 'all' : 'date';
    this.getMarcadores(TypeSearch, FilterFecha.trim());

  }

  getEmpleadoById(): void {
    this.empleadosService
      .getEmpleadoById(this.IdEmpleado)
      .subscribe((empleado) => this.empleadoCurrent = empleado);
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  nuevoMarcador() {
    this.router.navigate(['nuevo-marcador', this.empleadoCurrent.idempleados]);
  }

}
