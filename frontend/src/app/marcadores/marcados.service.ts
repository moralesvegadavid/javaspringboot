import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Marcados } from './marcados';

@Injectable({
  providedIn: 'root',
})
export class MarcadosService {
  private url: string = 'http://localhost:8080/api/v1/marcados';
  constructor(private http: HttpClient) {}
  /* Llamar a todos los Marcados */
  getAll(): Observable<Marcados[]> {
    return this.http.get<Marcados[]>(this.url);
  }
  /* Crear Empleado */
  create(marcado: Marcados): Observable<Marcados> {
    return this.http.post<Marcados>(this.url, marcado);
  }
  /* Llamar a un Empleado */
  getMarcadorById(id: number): Observable<Marcados> {
    return this.http.get<Marcados>(this.url + '/' + id);
  }
}
