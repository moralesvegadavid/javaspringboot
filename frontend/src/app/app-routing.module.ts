import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EmpleadosFormComponent } from './components/empleados-form/empleados-form.component';
import { EmpleadosComponent } from './empleados/empleados.component';
import { EmpleadosUpdateComponent } from './components/empleados-update/empleados-update.component';
import { MarcadoresComponent } from './marcadores/marcadores.component';
import { MarcadorFormComponent } from './components/marcador-form/marcador-form.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'empleados',
    pathMatch: 'full',
  },
  {
    path: 'empleados',
    component: EmpleadosComponent,
  },
  {
    path: 'form',
    component: EmpleadosFormComponent,
  },
  {
    path: 'update/:idempleados',
    component: EmpleadosUpdateComponent,
  },
  {
    path: 'marcadores/:idempleados',
    component: MarcadoresComponent,
  },
  {
    path: 'nuevo-marcador/:idempleados',
    component: MarcadorFormComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
