import { Component, OnInit } from '@angular/core';
import { MarcadosService } from '../../marcadores/marcados.service';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Marcados } from '../../marcadores/marcados';

@Component({
  selector: 'app-marcador-form',
  templateUrl: './marcador-form.component.html',
  styleUrls: ['./marcador-form.component.scss']
})
export class MarcadorFormComponent implements OnInit {

  marcados: Marcados = new Marcados();
  private IdEmpleado: number;

  constructor(
    private marcadosService: MarcadosService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.activatedRoute.params.subscribe((params: Params) => {
      this.IdEmpleado = params['idempleados'];
    });
  }

  create(): void {
    console.log(this.marcados);
    const NewMarcador = { ...this.marcados, idempleados: this.IdEmpleado };
    this.marcadosService
      .create(NewMarcador)
      .subscribe((res) => this.router.navigateByUrl(`/marcadores/${this.IdEmpleado}`));

  }
}

