import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Empleados } from 'src/app/empleados/empleados';
import { EmpleadosService } from '../../empleados/empleados.service';

@Component({
  selector: 'app-empleados-form',
  templateUrl: './empleados-form.component.html',
  styleUrls: ['./empleados-form.component.scss'],
})
export class EmpleadosFormComponent implements OnInit {

  empleados: Empleados = new Empleados();

  constructor(
    private empleadosService: EmpleadosService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) {}

  ngOnInit(): void {

  }

  create(): void {
    // console.log(this.empleados);
    this.empleadosService
      .create(this.empleados)
      .subscribe((res) => this.router.navigate(['/empleados']));
  }
}
