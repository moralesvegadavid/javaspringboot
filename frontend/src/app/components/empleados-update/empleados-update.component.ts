import { Component, OnInit } from '@angular/core';
import { Empleados } from '../../empleados/empleados';
import { EmpleadosService } from '../../empleados/empleados.service';
import { Router, ActivatedRoute, Params } from '@angular/router';

@Component({
  selector: 'app-empleados-update',
  templateUrl: './empleados-update.component.html',
  styleUrls: ['./empleados-update.component.scss']
})
export class EmpleadosUpdateComponent implements OnInit {

  public empleados: Empleados = new Empleados();
  public empleadoCurrent: Empleados = new Empleados();

  private IdEmpleado: number;

  constructor(
    private empleadosService: EmpleadosService,
    private router: Router,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {

    this.activatedRoute.params.subscribe((params: Params) => {
      this.IdEmpleado = params['idempleados'];
      this.getEmpleadoById();
    });

  }

  getEmpleadoById(): void {
    // console.log(this.empleados);
    this.empleadosService
      .getEmpleadoById(this.IdEmpleado)
      .subscribe((empleado) => {
        this.empleadoCurrent = empleado;
      });
  }

  updateEmpleado(): void {
    this.empleadosService
      .update(this.IdEmpleado, this.empleados)
      .subscribe((res) => this.router.navigate(['/empleados']));
  }


}
