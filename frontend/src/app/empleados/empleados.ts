export class Empleados {
  idempleados: number;
  primernombre: string;
  segundonombre: string;
  primerapellido: string;
  segundoapellido: string;
  carnetidentidad: string;
  fechanacimiento: string;
  fechaingreso: string;
}
