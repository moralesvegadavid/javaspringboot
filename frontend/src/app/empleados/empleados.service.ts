import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Empleados } from './empleados';

@Injectable({
  providedIn: 'root',
})
export class EmpleadosService {
  private url: string = 'http://localhost:8080/api/v1/empleados';
  constructor(private http: HttpClient) {}
  /* Llamar a todos los empleados */
  getAll(): Observable<Empleados[]> {
    return this.http.get<Empleados[]>(this.url);
  }
  /* Crear Empleado */
  create(empleado: Empleados): Observable<Empleados> {
    return this.http.post<Empleados>(this.url, empleado);
  }
  /* Llamar a un Empleado */
  getEmpleadoById(id: number): Observable<Empleados> {
    return this.http.get<Empleados>(this.url + '/' + id);
  }
  /* actualizar a un Empleado */
  update(id: number, empleado: Empleados): Observable<Empleados> {
    return this.http.put<Empleados>(`${this.url}/${id}`, empleado);
  }
  /* eliminar a un Empleado */
  delete(id: number): Observable<Empleados> {
    return this.http.delete<Empleados>(this.url + '/' + id);
  }
}
