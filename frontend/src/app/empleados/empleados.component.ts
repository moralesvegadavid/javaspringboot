import { Component, OnInit } from '@angular/core';
import { Empleados } from './empleados';
import { EmpleadosService } from './empleados.service';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';

@Component({
  selector: 'app-empleados',
  templateUrl: './empleados.component.html',
  styleUrls: ['./empleados.component.scss'],
})
export class EmpleadosComponent implements OnInit {
  empleados: Empleados[] = [];
  displayedColumns: string[] = [
    'idempleados',
    'primernombre',
    'segundonombre',
    'primerapellido',
    'segundoapellido',
    'carnetidentidad',
    'fechanacimiento',
    'fechaingreso',
    'acciones',
  ];
  dataSource!: MatTableDataSource<any>;

  constructor(
    private empleadosService: EmpleadosService,
    private router: Router) { }

  ngOnInit(): void {
    this.getEmpleados();
  }

  getEmpleados() {
    this.empleadosService.getAll().subscribe((e) => {
      this.empleados = e;
      this.dataSource = new MatTableDataSource(this.empleados);
    });
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  eliminarEmpleado(empleado: Empleados) {
    this.empleadosService.delete(empleado.idempleados)
      .subscribe((result: any) => this.getEmpleados());
  }

  actualizarEmpleado(empleado: Empleados) {
    this.router.navigate(['update', empleado.idempleados]);
  }

  marcadorEmpleado(empleado: Empleados) {
    this.router.navigate(['marcadores', empleado.idempleados]);
  }

}
