package xyz.ayaic.six.servicio.implementacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xyz.ayaic.six.modelo.Marcados;
import xyz.ayaic.six.repositorio.MarcadosRepositorio;
import xyz.ayaic.six.servicio.MarcadosServicio;

@Service
public class MarcadosServicioImplementacion implements MarcadosServicio {

    @Autowired
    MarcadosRepositorio marcadosRepositorio;

    @Override
    public List<Marcados> buscarTodosMarcados() {
        return marcadosRepositorio.findAll();
    }

    @Override
    public Marcados buscarPorId(Long id) {
        return marcadosRepositorio.findById(id).orElse(null);
    }

    @Override
    public void adicionar(Marcados dato) {
        marcadosRepositorio.save(dato);      
    }
    
}
