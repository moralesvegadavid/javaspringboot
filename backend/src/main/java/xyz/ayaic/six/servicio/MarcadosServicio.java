package xyz.ayaic.six.servicio;

import java.util.List;

import xyz.ayaic.six.modelo.Marcados;

public interface MarcadosServicio {
    
    List<Marcados> buscarTodosMarcados();

    Marcados buscarPorId(Long id);

    void adicionar(Marcados dato);


}
