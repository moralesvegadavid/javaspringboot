package xyz.ayaic.six.servicio;

import java.util.List;

import xyz.ayaic.six.modelo.Empleados;

public interface EmpleadosServicio {

    List<Empleados> buscarTodos();

    Empleados buscarPorId(Long id);

    void adicionar(Empleados dato);

    Empleados mdificar(Long id,Empleados dato);

    void borrar(Long id);
    
}