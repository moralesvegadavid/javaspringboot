package xyz.ayaic.six.servicio.implementacion;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import xyz.ayaic.six.modelo.Empleados;
import xyz.ayaic.six.repositorio.EmpleadosRepositorio;
import xyz.ayaic.six.servicio.EmpleadosServicio;

@Service
public class EmpleadosServicioImplementacion implements EmpleadosServicio{

    @Autowired
    EmpleadosRepositorio empleadosRepositorio;

    @Override
    public List<Empleados> buscarTodos() {
        return empleadosRepositorio.findAll();
    }

    @Override
    public Empleados buscarPorId(Long id) {
        return empleadosRepositorio.findById(id).orElse(null);
    }

    @Override
    public void adicionar(Empleados dato) {
        empleadosRepositorio.save(dato);
    }

    @Override
    public Empleados mdificar(Long id,Empleados dato) {
        Empleados empleadoFromDB = empleadosRepositorio
            .findById(id)
            .orElseThrow(RuntimeException::new);

        empleadoFromDB.setPrimernombre(dato.getPrimernombre());
        empleadoFromDB.setSegundonombre(dato.getSegundonombre());
        empleadoFromDB.setPrimerapellido(dato.getPrimerapellido());
        empleadoFromDB.setSegundoapellido(dato.getSegundoapellido());
        empleadoFromDB.setCarnetidentidad(dato.getCarnetidentidad());
        empleadoFromDB.setFechanacimiento(dato.getFechanacimiento());
        empleadoFromDB.setFechaingreso(dato.getFechaingreso());

        return empleadosRepositorio.save(empleadoFromDB);
    }

    @Override
    public void borrar(Long id) {
        empleadosRepositorio.deleteById(id);
    }
    
}
