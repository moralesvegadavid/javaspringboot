package xyz.ayaic.six.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;

import xyz.ayaic.six.modelo.Empleados;

public interface EmpleadosRepositorio extends JpaRepository<Empleados, Long>{
    
}
