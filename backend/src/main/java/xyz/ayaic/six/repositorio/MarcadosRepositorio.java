package xyz.ayaic.six.repositorio;

import org.springframework.data.jpa.repository.JpaRepository;

import xyz.ayaic.six.modelo.Marcados;

public interface MarcadosRepositorio extends JpaRepository<Marcados,Long>{
    
}
