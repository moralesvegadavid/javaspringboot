package xyz.ayaic.six.modelo;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="empleados")
public class Empleados {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idempleados;
    private String primernombre;
    private String segundonombre;
    private String primerapellido;
    private String segundoapellido;
    private String carnetidentidad;
    private Date fechanacimiento;
    private Date fechaingreso; 

   

    /* @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idempleados;
    private String primernombre;
    private String segundonombre;
    private String primerapellido;
    private String segundoapellido;
    private String carnetidentidad; */

    /* @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idpersona;
    private String primerapellido;
    private String primernombre;
    private Long idgenero; */
}