package xyz.ayaic.six.modelo;

import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="marcados")
public class Marcados {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idmarcados;
    private String carnetidentidad;
    private Date fecha;
    private Date hora;
    private String observacion;
    private Long idempleados;

    
}
