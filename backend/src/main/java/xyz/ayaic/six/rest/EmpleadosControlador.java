package xyz.ayaic.six.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
// import io.swagger.v3.oas.annotations.tags.Tag;
import xyz.ayaic.six.modelo.Empleados;
import xyz.ayaic.six.servicio.EmpleadosServicio;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1/empleados")
// @Tag(description = "Tiene la funcionalidad de la Tabla Empleados", name = "Controlador de Empleados")
public class EmpleadosControlador {

    @Autowired
    EmpleadosServicio empleadosServicio;

    @GetMapping
    @Operation(summary = "Buscar Todos los Elementos de los Empleados")
    ResponseEntity<?> buscarTodos() {
        return new ResponseEntity<List<Empleados>>(empleadosServicio.buscarTodos(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    ResponseEntity<?> buscarPorId(
        @PathVariable Long id
    ) {
        return new ResponseEntity<Empleados>(empleadosServicio.buscarPorId(id), HttpStatus.OK);
    }

    @PostMapping
    ResponseEntity<?> adicionar(
        @RequestBody Empleados dato
    ) {
        empleadosServicio.adicionar(dato);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PutMapping
    ResponseEntity<?> modificar(
        @PathVariable Long id,
        @RequestBody Empleados dato
    ) {
        empleadosServicio.mdificar(id,dato);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    ResponseEntity<?> borrar(
        @PathVariable Long id
    ) {
        empleadosServicio.borrar(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
    
}