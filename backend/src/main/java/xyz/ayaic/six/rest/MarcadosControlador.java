package xyz.ayaic.six.rest;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

// import io.swagger.v3.oas.annotations.Operation;
import xyz.ayaic.six.modelo.Marcados;
// import xyz.ayaic.six.servicio.MarcadosServicio;
import xyz.ayaic.six.servicio.MarcadosServicio;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping("/api/v1/marcados")
public class MarcadosControlador {
    
  
    // @Autowired(required = false)
    @Autowired(required = false)
    MarcadosServicio marcadosServicio;

    @GetMapping
    // @Operation(summary = "Buscar Todos los Elementos de los Empleados")
    ResponseEntity<?> buscarTodos() {
        return new ResponseEntity<List<Marcados>>(marcadosServicio.buscarTodosMarcados(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    ResponseEntity<?> buscarPorId(
        @PathVariable Long id
    ) {
        return new ResponseEntity<Marcados>(marcadosServicio.buscarPorId(id), HttpStatus.OK);
    }

    @PostMapping
    ResponseEntity<?> adicionar(
        @RequestBody Marcados dato
    ) {
        marcadosServicio.adicionar(dato);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

}
