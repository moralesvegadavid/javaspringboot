create table empleados (
    idempleados serial not null primary key,
    primerNombre varchar,
    segundoNombre varchar,
    primerApellido varchar,
    segundoApellido varchar,
    carnetIdentidad varchar,
    fechaNacimiento date,
    fechaIngreso date    
);

create table marcados(
    idmarcados serial not null primary key,
    carnetIdentidad varchar,
    fecha	date,
    hora	date,
    observacion varchar,
    idempleados integer,
    foreign key (idempleados) references empleados (idempleados)
    
);
