# PROYECTO AYAIC RRHH

#### Una aplicación web de RR.HH Utiliza Spring Boot y Angular .

Esta es una aplicación con representación del lado del frontend y backend.
El cliente frontend realiza llamadas API al servidor backend cuando se está ejecutando.

## Esquema de base de datos

## Cómo ejecutar

Inicie el servidor backend antes que el cliente frontend.

**BACKEND**

1. cd backend .
2. Ejecute `mvn install` .
3. Ejecute `mvn spring-boot:run` .
4. El servidor backend se ejecuta en [localhost:8080]().

**FRONTEND**

1. Instale [ Node.js y npm ] (https://www.npmjs.com/get-npm)
2. cd frontend .
3. Ejecute `npm install` .
4. ng serve -o

Nota: La URL de la API de backend está configurada en `src/environments/environment.ts` del proyecto frontend. Es `localhost:8080/api` por defecto.
